# e-odbo-android-sample

#### 介绍
e-odbo库的Android应用示例，可以和桌面端一样使用e-odbo库。


#### 使用说明

1. 新建数据库
  ```
  public class TestDatabase extends DataBase {
      static Table user=new Table("user").implement(PK.PK_String, TimeLogAble.Instance)
              .Column("name", ColumnType.VARCHAR(60), ColumnFlag.build(ColumnFlag.NotNull))
              .Column("age", ColumnType.INT)
              .Column("addr",ColumnType.VARCHAR(255))
              .Column("tel",ColumnType.VARCHAR(15));

      public TestDatabase() {
          super("1.0.0");
          addTable(user);
      }

      public static Table getUserTable() {
          return user;
      }
  }
  ```

2. 使用工具类生成 实体类 及 DAO 类
```
public class TestDatabaseTest {

    @Test
    public void getUserTable() {
        Table userTable=TestDatabase.getUserTable();
        TableUtils.printTableDAO(userTable);
        TableUtils.printTableBean(userTable);
    }
}
```

3. 在App中初始化 数据库 及 entryManager
```
public class App extends Application {

    public DB localDB;

    private EntityManager entityManager;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init(){
        TestDatabase database=new TestDatabase();
        try {
            localDB= SQLite.androidWithSQLDroid(this.getPackageName(),"test",null,null);
            Log.d("e","setup database start....");
            localDB.setup(database);
        }catch (BasicException e){
            Log.e("e","setup database false",e);
        }
        localDB.model(database);
        Log.d("e","init database end....");
        String url = String.format("jdbc:sqldroid:%s","/data/data/" + getPackageName() +"/databases/test.db");
        DBSession dbSession=null;
        try {
             dbSession=new DBSession(url,null,null);
        } catch (BasicException e) {
            e.printStackTrace();
        }
        entityManager=new EntityManager(dbSession,new TableDBSentenceBuilder(dbSession,new NoScuritySupportManager()));
        entityManager.addDaoToMap(new UserDAO(dbSession));

    }

    public DB getLocalDB() {
        return localDB;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
```

4.在其它Activity中引用entityManager操作数据库
```
      //新增
       User user=new User();
        user.setAddr("test addr");
        user.setAge(10);
        user.setName("test name"+random.nextInt(10000));
        user.setTel("110-225000");
        user.setCreateDate(new Date());
        try {
            entryManager.insert(user);
        } catch (BasicException e) {
            e.printStackTrace();
        }
        //查询
        int count=-1;
        UserDAO userDAO= (UserDAO) entryManager.getDao(User.class);
         try {
              count=userDAO.count(QBF.all());
          } catch (BasicException e) {
              e.printStackTrace();
          }
          textView.setText("总计：\n \t"+count+"条记录");
```

