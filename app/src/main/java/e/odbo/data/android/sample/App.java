package e.odbo.data.android.sample;

import android.app.Application;
import android.util.Log;

import com.openbravo.data.basic.BasicException;
import com.openbravo.data.loader.DBSession;
import com.openbravo.data.loader.TableDBSentenceBuilder;
import com.openbravo.data.loader.dialect.SQLite.SQLite;

import java.util.Date;
import java.util.List;

import e.odbo.DB;
import e.odbo.data.android.sample.dao.UserDAO;
import e.odbo.data.android.sample.entry.User;
import e.odbo.data.android.sample.model.TestDatabase;
import e.odbo.data.dao.EntityManager;
import e.odbo.data.dsl.query.QBF;
import e.odbo.data.sample.security.NoScuritySupportManager;

public class App extends Application {

    public DB localDB;

    private EntityManager entityManager;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init(){
        TestDatabase database=new TestDatabase();
        try {
            localDB= SQLite.androidWithSQLDroid(this.getPackageName(),"test",null,null);
            Log.d("e","setup database start....");
            localDB.setup(database);
        }catch (BasicException e){
            Log.e("e","setup database false",e);
        }
        localDB.model(database);
        Log.d("e","init database end....");
        String url = String.format("jdbc:sqldroid:%s","/data/data/" + getPackageName() +"/databases/test.db");
        DBSession dbSession=null;
        try {
             dbSession=new DBSession(url,null,null);
        } catch (BasicException e) {
            e.printStackTrace();
        }
        entityManager=new EntityManager(dbSession,new TableDBSentenceBuilder(dbSession,new NoScuritySupportManager()));
        entityManager.addDaoToMap(new UserDAO(dbSession));

    }

    public DB getLocalDB() {
        return localDB;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }


}
