package e.odbo.data.android.sample;

import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.openbravo.data.basic.BasicException;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import e.odbo.data.android.sample.ui.state.StateFragment;
import e.odbo.data.android.sample.ui.user.UserEditFragment;
import e.odbo.data.android.sample.ui.user.UserEditViewModel;
import e.odbo.data.android.sample.ui.user.UserListViewModel;
import e.odbo.data.android.sample.ui.user.UserlistFragment;
import e.odbo.data.android.sample.entry.User;
import e.odbo.data.dao.EntityManager;

public class MainActivity extends AppCompatActivity implements UserlistFragment.OnListFragmentInteractionListener {

    private TextView mTextMessage;

    EntityManager entryManager;

    UserListViewModel userListViewModel;

    UserEditViewModel userEditViewModel;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText("状态");
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new StateFragment()).commit();
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new UserlistFragment()).commit();
                    return true;
                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    testUserDao();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        entryManager=((App) this.getApplication()).getEntityManager();
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        userListViewModel = ViewModelProviders.of(this).get(UserListViewModel.class);
        userEditViewModel =ViewModelProviders.of(this).get(UserEditViewModel.class);
        List<User> userList=null;
         try {
             userList=entryManager.list(User.class);
         } catch (BasicException e) {
             e.printStackTrace();
             userList=new ArrayList<>();
         }

         userListViewModel.getUserList().setValue(userList);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new StateFragment()).commit();
    }

    @Override
    public void onListFragmentInteraction(User item) {
        Log.d("d","select user item:"+item.getName());
        userEditViewModel.getCurrentUser().setValue(item);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new UserEditFragment()).commit();
    }

    Random random=new Random();
    private void testUserDao(){
        User user=new User();
        user.setAddr("test addr");
        user.setAge(10);
        user.setName("test name"+random.nextInt(10000));
        user.setTel("110-225000");
        user.setCreateDate(new Date());
        try {
            entryManager.insert(user);
        } catch (BasicException e) {
            e.printStackTrace();
        }

        userListViewModel.add(user);
        userEditViewModel.getCurrentUser().setValue(user);
    }
}
