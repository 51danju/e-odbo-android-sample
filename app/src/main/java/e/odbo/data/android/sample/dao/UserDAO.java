package e.odbo.data.android.sample.dao;

import com.openbravo.data.basic.BasicException;
import com.openbravo.data.loader.I_Session;
import com.openbravo.data.loader.serialize.DataRead;
import com.openbravo.data.loader.serialize.DataWrite;
import com.openbravo.data.loader.serialize.Datas;

import e.odbo.data.android.sample.entry.User;
import e.odbo.data.dao.ModifiedLogDAO;
import e.odbo.data.dao.table.Field;
import e.odbo.data.dao.table.TableDefinition;
import e.odbo.data.format.Formats;

public class UserDAO extends ModifiedLogDAO<User> {

    public UserDAO(I_Session s) {
        super(s);
    }

    @Override
    public TableDefinition getTable() {
        TableDefinition userTableDefinition=new TableDefinition("user" ,new Field[]{
                new Field(User.ID, Datas.STRING, Formats.STRING),//(PK)
                new Field(User.CURDATE,Datas.TIMESTAMP,Formats.TIMESTAMP),//(notNull)
                new Field(User.LASTMODIFIED,Datas.TIMESTAMP,Formats.TIMESTAMP),//
                new Field(User.NAME,Datas.STRING,Formats.STRING),//(notNull)
                new Field(User.AGE,Datas.INT,Formats.INT),//
                new Field(User.ADDR,Datas.STRING,Formats.STRING),//
                new Field(User.TEL,Datas.STRING,Formats.STRING)//
        }, new int[] {0}
        );
        return userTableDefinition;
    }

    @Override
    public void writeInsertValues(DataWrite dataWrite, User s) throws BasicException {
          dataWrite.setString(1,s.getKey());
          dataWrite.setTimestamp(2,s.getCreateDate());
          dataWrite.setTimestamp(3,s.getLastModified());
          dataWrite.setString(4,s.getName());
          dataWrite.setInt(5,s.getAge());
          dataWrite.setString(6,s.getAddr());
          dataWrite.setString(7,s.getTel());
    }

    @Override
    public Class getSuportClass() {
        return User.class;
    }

    @Override
    public User readValues(DataRead dataRead, User s) throws BasicException {
        if(s==null)
            s=new User();
        s.setKey(dataRead.getString(1));
        s.setCreateDate(dataRead.getTimestamp(2));
        s.setLastModified(dataRead.getTimestamp(3));
        s.setName(dataRead.getString(4));
        s.setAge(dataRead.getInt(5));
        s.setAddr(dataRead.getString(6));
        s.setTel(dataRead.getString(7));
        return s;
    }
}