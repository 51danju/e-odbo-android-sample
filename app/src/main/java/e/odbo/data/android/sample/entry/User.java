package e.odbo.data.android.sample.entry;


import java.util.Date;

import e.odbo.data.bean.AutoGreneratorStringKeyModifiedLogBean;

public class User extends AutoGreneratorStringKeyModifiedLogBean{

    public final static String ID="ID";
    public final static String CURDATE="CURDATE";
    public final static String LASTMODIFIED="LASTMODIFIED";
    public final static String NAME="NAME";
    public final static String AGE="AGE";
    public final static String ADDR="ADDR";
    public final static String TEL="TEL";

    private String name;
    private int age;
    private String addr;
    private String tel;
    private Date createDate;

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
