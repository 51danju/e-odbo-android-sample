package e.odbo.data.android.sample.model;


import java.util.Date;

import e.odbo.data.model.Table;
import e.odbo.data.model.TableData;

public class SmapleData {

    public static TableData[] getSampleDatas_zh(){
        Table user= TestDatabase.getUserTable();
        TableData tableData=TableData.C(user.getName(),user.getColumnNames()).types(user.getColumnDatas())
                .value("1",new Date(),"张三",18,"浙江省杭州市上城区","18855555521")
                .value("2",null,"李四",26,"广东省广州市越秀区中山六路","0511-21323211");
        return new TableData[]{tableData};
    }

    public static TableData[] getSampleDatas_en(){
        Table user= TestDatabase.getUserTable();
        TableData tableData=TableData.C(user.getName(),user.getColumnNames()).types(user.getColumnDatas());
        return new TableData[]{tableData};
    }
}
