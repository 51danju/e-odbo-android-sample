package e.odbo.data.android.sample.model;


import e.odbo.data.model.ColumnFlag;
import e.odbo.data.model.ColumnType;
import e.odbo.data.model.DataBase;
import e.odbo.data.model.Table;
import e.odbo.data.model.smaple.PK;
import e.odbo.data.model.smaple.TimeLogAble;

public class TestDatabase extends DataBase {
    static Table user=new Table("user").implement(PK.PK_String, TimeLogAble.Instance)
            .Column("name", ColumnType.VARCHAR(60), ColumnFlag.build(ColumnFlag.NotNull))
            .Column("age", ColumnType.INT)
            .Column("addr",ColumnType.VARCHAR(255))
            .Column("tel",ColumnType.VARCHAR(15));

    public TestDatabase() {
        super("1.0.0");
        addTable(user);
    }

    public static Table getUserTable() {
        return user;
    }


}
