package e.odbo.data.android.sample.ui.user;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import e.odbo.data.android.sample.R;
import e.odbo.data.android.sample.entry.User;

public class UserEditFragment extends Fragment {

    @BindView(R.id.saveButton)
    Button saveButton;
    @BindView(R.id.addButton)
    Button addButton;
    @BindView(R.id.removeButtom)
    Button removeButtom;
    @BindView(R.id.resertButton)
    Button resertButton;
    @BindView(R.id.toolbar)
    LinearLayout toolbar;
    @BindView(R.id.edit_userName)
    EditText editUserName;
    @BindView(R.id.edit_userAge)
    SeekBar editUserAge;
    @BindView(R.id.edit_userAddr)
    EditText editUserAddr;
    @BindView(R.id.edit_userTel)
    EditText editUserTel;
    @BindView(R.id.memo)
    EditText memo;
    private UserEditViewModel mViewModel;

    private Unbinder unbinder;

    public static UserEditFragment newInstance() {
        return new UserEditFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.user_edit_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(UserEditViewModel.class);
        mViewModel.getCurrentUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                reflash(user);
            }
        });
    }

    @OnClick({R.id.saveButton, R.id.addButton, R.id.removeButtom, R.id.resertButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.saveButton:
                break;
            case R.id.addButton:
                break;
            case R.id.removeButtom:
                break;
            case R.id.resertButton:
                break;
        }
    }


    private void reflash(User user){
        if(user==null)
            return;
        this.editUserName.setText(user.getName());
        this.editUserAge.setProgress(user.getAge());
        this.editUserAddr.setText(user.getAddr());
        this.editUserTel.setText(user.getTel());
    }

    private User build(User user){
        user.setName(editUserName.getText().toString());
        user.setAge(editUserAge.getProgress());
        user.setAddr(editUserAddr.getText().toString());
        user.setTel(editUserTel.getText().toString());
        return user;
    }

}
