package e.odbo.data.android.sample.ui.user;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import e.odbo.data.android.sample.entry.User;

public class UserEditViewModel extends ViewModel {
    MutableLiveData<User> currentUser=new  MutableLiveData<>();

    public MutableLiveData<User> getCurrentUser() {
        return currentUser;
    }
}
