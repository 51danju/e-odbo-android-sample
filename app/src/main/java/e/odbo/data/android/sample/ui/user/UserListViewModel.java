package e.odbo.data.android.sample.ui.user;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import e.odbo.data.android.sample.entry.User;

public class UserListViewModel extends ViewModel {
    private MutableLiveData<List<User>> userList = new MutableLiveData<>();

    public MutableLiveData<List<User>> getUserList() {
        return userList;
    }

    public void add(User user){
        userList.getValue().add(user);
        userList.setValue(userList.getValue());
    }
}
