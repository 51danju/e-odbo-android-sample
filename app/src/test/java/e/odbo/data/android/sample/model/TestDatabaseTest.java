package e.odbo.data.android.sample.model;

import org.junit.Test;

import e.odbo.data.model.Table;
import e.odbo.data.util.TableUtils;

import static org.junit.Assert.*;

public class TestDatabaseTest {

    @Test
    public void getUserTable() {
        Table userTable=TestDatabase.getUserTable();
        TableUtils.printTableDAO(userTable);
        TableUtils.printTableBean(userTable);
    }
}